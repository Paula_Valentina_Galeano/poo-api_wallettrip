//Importar la clase express

import express from 'express';

import agregar_sitio from './agregar_sitio.js';
import get_sitio from './get_sitio.js';
import get_lugar from './get_lugar.js'


//Crear un objeto express
const app=express();
const puerto=3001;

// Crear una ruta

app.get("/Bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});


app.get("/Agregar_sitio", (req, res)=>{

    res.send(agregar_sitio());
    
});


app.get("/Consultar_sitio", (req, res)=>{

    res.send(get_sitio(req));
    
});

app.get("/Consultar_lugar", (req, res)=>{

    res.send(get_lugar(req));
    
});


//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});


