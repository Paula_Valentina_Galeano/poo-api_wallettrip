
import mysql from "mysql";

export default function get_lugar(req) {

  //1. definir los parámetros de conexión a mysql

  let conexion;

  let parametros = {
    host: "localhost",
    user: "root",
    password: "20191025018",
    database: "wallettrip_db",
  };
  conexion = mysql.createConnection(parametros);

  //2. Conectarnos al servidor mysql

  conexion.connect(function (err) {
    if (err) {
      console.log("Error al conectarse al servidor" + err.message);
      return false;
    } else {
      console.log("Conectado al servidor MySQL");
      return true;
    }
  });


  //3. Realizar consulta SQL
  
  let consulta = `SELECT idplace, name FROM place WHERE idplace=?`;
  let lugar;

  conexion.query(consulta, [req.query.a], (err, results, fields) => {
    if (err) {
      console.error("Error al realizar la consulta" + err.message);
    } else {
        lugar = results;
        console.log (lugar)
    };
  });
  

  //4. Desconectarnos al servidor mysql
  
  conexion.end();
  return JSON.stringify (lugar);

}



