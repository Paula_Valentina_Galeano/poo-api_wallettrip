
import mysql from 'mysql';


export default function agregar_sitio(){

    //1. definir los parámetros de conexión a mysql

    let conexion;

    let parametros = {
        host: "localhost",
        user: "root",
        password: "20191025018",
        database: "wallettrip_db",
      };
    conexion=mysql.createConnection(parametros);

    //2. Conectarnos al servidor mysql

    conexion.connect(function (err){
        if(err){
            console.log("Error al conectarse al servidor" + err.message);
            return false;
        }else{
            console.log("Conectado al servidor MySQL");
            return true;
        }
    });

    //3. Realizar consulta SQL

    let consulta=`INSERT INTO place (idplace, name, description, country_id, region, location) 
    VALUES (6,
        'Járdin Botánico de Bogotá', 
        'Espacio encantador por la vistosidad de su flora',
        '57', 
        'Bogotá, D.C',
        ST_GeomFromText('POINT(4.611050940917936 -74.07046796181973)')
        ) `;

        conexion.query(consulta);


    //4. Desconectarnos al servidor mysql

    conexion.end();

    return "Insertado!!";

}



